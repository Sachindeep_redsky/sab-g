import { Component, OnInit, AfterContentInit, Type } from "@angular/core";
import { Page } from "tns-core-modules/ui/page/page";
import * as firebase from "nativescript-plugin-firebase";
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { RouterExtensions } from 'nativescript-angular/router/router-extensions';
import { Values } from "~/app/values/values";
import * as localstorage from "nativescript-localstorage";

@Component({
    selector: "ns-login",
    moduleId: module.id,
    templateUrl: "./login.component.html",
    styleUrls: ['./login.component.css']
})

export class LoginComponent implements OnInit, AfterContentInit {

    // isRendering: boolean;
    isLoading: boolean;
    constructor(private routerExtensions: RouterExtensions, private page: Page, private http: HttpClient) {
        
        this.page.actionBarHidden = true;
        this.isLoading = false;
      
    }

    ngAfterContentInit(): void {
        // this.renderingTimeout = setTimeout(() => {
        //     this.isRendering = true;
        // }, 5000)
    }
    ngOnInit(): void {
        // throw new Error("Method not implemented.");
    }

    onGoogleLogin() {
        this.isLoading=true;
        firebase.login({
            type: firebase.LoginType.GOOGLE
        }).then((result) => {
            JSON.stringify(result);
            console.log('google result', result);
            if (result && result.providers) {
                result.providers.forEach(element => {
                    if (element.token !== null && element.token !== undefined) {
                        setTimeout(() => {
                            this.isLoading=true;
                            // var _name = result.displayName.split(' ', 1);
                            // var _userName = _name[0].toString();
                            let user = {
                                uId: result.uid,
                                tempToken: element.token,
                                loginType: 'google',
                                email: result.email,
                                userName:  result.displayName,
                                type:'user'
                            }
                            console.log("Google Data....", user)
                            this.http.post(Values.BASE_URL + `users`, user).subscribe((res: any) => {
                                console.trace("google:::SIGNUP", res)
                                if (res && res.isSuccess && res.data) {
                                    localstorage.setItem("token",res.data.token);
                                    localstorage.setItem("userId",res.data._id);
                                    localstorage.setItem("userName",res.data.userName);
                                    localstorage.setItem("Gmail",res.data.email);
                                    localstorage.setItem("cartId",res.data.cartId);
                                    this.isLoading = false;
                                    this.routerExtensions.navigate(['/home'], { clearHistory: true })
                                }
                            },
                             (error) => {
                                console.log('Error:', error);
                                this.isLoading = false;
                                if (error.error.statusCode == 422 && error.error.error == "user_already_exists") {
                                    alert("User already exists");
                                } else {
                                    alert("Something went wrong");
                                this.isLoading = false;

                                }
                            })
                        }, 100)
                        
                    }
                });
            }
            else {
                alert("Something went wrong Please try later");
            this.isLoading = false;

            }
        }, (errorMessage) => {
            this.isLoading = false;
            // this.userService.showErrorDialog(true, "Please check your network connection.");
            // alert(errorMessage)
            console.log(errorMessage);
        }
        );
    }

}
